import { InjectRepository } from "@nestjs/typeorm";
import { Injectable, BadRequestException, UnauthorizedException, Post, Body } from "@nestjs/common";
import { Repository } from "typeorm";

import { News } from "../model/news.entity";

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>
  ) {}

  async getNews() {
    return await this.newsRepository.find();
  }

  @Post()
  async create(payload) {
    const news: News = new News();
    
      news.title = payload.title
    
    return await this.newsRepository.save(news);
  }
}
