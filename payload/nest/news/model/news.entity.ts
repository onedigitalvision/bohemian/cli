import { Entity, Column, PrimaryGeneratedColumn, Timestamp } from 'typeorm';

import { IsEmail, IsNotEmpty, Equals } from 'class-validator';

@Entity('news')
export class News {
  @PrimaryGeneratedColumn()
  id: number;  
  
  @Column({ length: 255 })
  @IsNotEmpty()
  title: varchar;
  

  @Column('timestamp', { default: () => `now()` })
  createdAt: Timestamp

  @Column('timestamp', { default: () => "CURRENT_TIMESTAMP")})
  updateAt: Timestamp

}