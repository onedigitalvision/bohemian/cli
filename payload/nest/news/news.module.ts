import { NewsService } from './news/news.service';
import { News } from './model/news.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([News])],
  providers: [NewsService],
  exports: [NewsService]
})
export class NewsModule {}   