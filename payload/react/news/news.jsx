import React, { Component } from 'react'
import { FusePageSimple, DemoContent } from '@fuse'
import ReactTable from 'react-table'
import { withRouter } from 'react-router-dom'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faTwitter,
  faFacebook,
  faLinkedin,
} from '@fortawesome/free-brands-svg-icons'

import {
  withStyles,
  Button,
  Divider,
  Typography,
  InputAdornment,
  Icon,
  Tooltip,
  Chip,
} from '@material-ui/core'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import NewNews from './crud/new'

import * as newsActions from './store/actions/news.actions'

const styles = theme => ({
  layoutRoot: {
    minHeight: '96vh',
  },
  button: {
    margin: theme.spacing.unit,
  },
  error: {
    background: theme.palette.error.dark,
  },
  toe: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  title: {
    textAlign: 'center',
    fontWeight: '700',
    width: '100%',
  },
})

class News extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  componentDidMount() {
    this.props.getNewss()
    // debugger
  }
  render() {
    const { classes } = this.props
    const data = []
    let that = this
    return (
      <FusePageSimple
        classes={{
          root: classes.layoutRoot,
        }}
        header={
          <div className="p-24">
            <h2>News Management</h2>
          </div>
        }
        contentToolbar={
          <div className="px-24">
            <div>
              <Button
                variant="contained"
                color="primary"
                onClick={this.handleClickOpen}
                className={classes.button}
              >
                Add new News
              </Button>
            </div>
          </div>
        }
        content={
          <div className="p-24">
            <ReactTable
              data={this.props.news.news}
              columns={[
                {
                  Header: 'id',
                  accessor: 'id',
                  maxWidth: 50,
                },
                {
                  Header: 'Title',
                  accessor: 'title',
                  Cell: props => (
                    <div
                      className={classes.title}
                      dangerouslySetInnerHTML={{ __html: props.value }}
                    />
                  ),
                },
                
                {
                  Header: 'Operations',
                  maxWidth: 250,
                  accessor: 'edit',
                  Cell: props => (
                    <div>
                      <Button
                        variant="contained"
                        color="error"
                        className={classes.button}
                      >
                        edit
                      </Button>
                      <Button
                        variant="contained"
                        color="error"
                        className={classes.error}
                      >
                        Delete
                      </Button>
                    </div>
                  ),
                },
              ]}
              defaultPageSize={10}
              className="-striped -highlight"
            />
            <NewNews
              open={this.state.open}
              handleClose={e => this.handleClose()}
              saveNews={e => this.saveNews(e)}
            />
          </div>
        }
      />
    )
  }

  saveNews(news) {
      this.props.createNews(news)
      this.handleClose();
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getNewss: newsActions.getNewss,
      createNews: newsActions.createNews
    },
    dispatch
  )
}

function mapStateToProps(news) {
  return 
    newss: news.newss
  }
}
export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(News)
)
