import EntityGenerator from './entitiies/entity_generator';

const {
  prompt
} = require('inquirer');

export default class CLI {
  constructor() {
    this.answers = {
      entities: []
    }; 
  }
  
  async init() {
    console.log(`%c
    _______             __                                __                             ______   __        ______ 
   |       |           |  |                              |  |                           /      | |  |      |      |
   | $$$$$$$  ______  |  $$____    ______   ______ ____   |$$  ______   _______        |  $$$$$$|| $$       |$$$$$$
   | $$__/ $$ /      | | $$    |  /      | |      |    | |  | |      | |       |       | $$   |$$| $$        | $$  
   | $$    $$|  $$$$$$|| $$$$$$$||  $$$$$$|| $$$$$$|$$$$|| $$  |$$$$$$|| $$$$$$$|      | $$      | $$        | $$  
   | $$$$$$$|| $$  | $$| $$  | $$| $$    $$| $$ | $$ | $$| $$ /      $$| $$  | $$      | $$   __ | $$        | $$  
   | $$__/ $$| $$__/ $$| $$  | $$| $$$$$$$$| $$ | $$ | $$| $$|  $$$$$$$| $$  | $$      | $$__/  || $$_____  _| $$_ 
   | $$    $$ |$$    $$| $$  | $$ |$$     || $$ | $$ | $$| $$ |$$    $$| $$  | $$       |$$    $$| $$     ||   $$ |
   | $$$$$$$  |$$$$$$  |$$   |$$  |$$$$$$$ |$$  |$$  |$$ |$$  |$$$$$$$ |$$   |$$        |$$$$$$  |$$$$$$$$ |$$$$$$
                                                                                                                                                     
   `);

   console.dir(this.answers)
   const menu = await prompt({
    type: 'list',
    name: 'action',
    choices: ['generate an entity', 'generate an api end point', 'generate the cms crud', 'exit'],
    message: 'I would like to?'
  });

  switch (menu.action) {
    case 'generate an entity':
      new EntityGenerator(prompt, this.answers, this).init()
  }

    
  }

}
