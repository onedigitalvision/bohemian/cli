import { writeFileSync, existsSync, mkdirSync } from 'fs';

const nunjucks = require('nunjucks');
const NunjucksInspect = require('nunjucks-inspect');
 
const  env = new nunjucks.Environment();
env.addExtension('inspect', new NunjucksInspect());
export default class EntityBuilder {

  constructor(entity) {
    this.entity = entity;


  }

  build() {
    this.constructModule();
    this.constructModel();
    this.constructService();
    this.constructPage();
  }

  constructModule() {
    nunjucks.render('templates/nestjs/{entity}/{entity}.module.ts.njk', this.entity, (err, res) => {
      const dir = `payload/nest/${this.entity.name}/`;
      if (!existsSync(dir)) {
        mkdirSync(dir,{recursive: true});
      }
      writeFileSync(`${dir}/${this.entity.name}.module.ts`, res);
    })
  }

  constructModel() {
    nunjucks.render('templates/nestjs/{entity}/model/{entity}.entity.ts.njk', this.entity, (err, res) => {      
      const dir = `payload/nest/${this.entity.name}/model`;
      if (!existsSync(dir)) {
        mkdirSync(dir,{recursive: true});
      }
      writeFileSync(`${dir}/${this.entity.name}.entity.ts`, res);
    })
  }

  constructService() {
    nunjucks.render('templates/nestjs/{entity}/{entity}/{entity}.service.ts.njk', this.entity, (err, res) => {
      const dir = `payload/nest/${this.entity.name}/${this.entity.name}`;
      if (!existsSync(dir)) {
        mkdirSync(dir,{recursive: true});
      }
      writeFileSync(`${dir}/${this.entity.name}.service.ts`, res);
    })
  }

  constructPage() {
    console.dir(this.entity)
    nunjucks.render('templates/react/{entity}.page.jsx.njk', this.entity, (err, res) => {
      console.error(err)
      console.dir(res)
      const dir = `payload/react/${this.entity.name}`;
      if (!existsSync(dir)) {
        mkdirSync(dir,{recursive: true});
      }
      writeFileSync(`${dir}/${this.entity.name}.jsx`, res);
    })
  }
}