import { writeFileSync } from "fs";
import EntityBuilder from "./entity_builder";

export default class EntityGenerator {

  constructor(prompt, answers, cli) {
    this.answers = answers;
    this.prompt = prompt;
    this.cli = cli;
    this.entity = {}
  }

  async init() {

    const entityRes = await this.prompt({
      type: 'input',
      name: 'entity',
      message: 'What is the new entity called?'
    });

    this.entity.name = entityRes.entity

    this.entity.attributes = [];

    this.addAttributes();
    
  }

  async addAttributes() {
    if (this.entity.attributes.length === 0) {
      this.entity.attributes.push(await this.addAttribute());
    }
    const res = await this.prompt({
      type: 'confirm',
      name: 'addAttr',
      default: true,
      message: 'Add another attriibute?'
    });
    if (res.addAttr) {
      this.entity.attributes.push(await this.addAttribute());
      this.addAttributes();
    } else {
      this.answers.entities.push(this.entity);
      writeFileSync('bohemian.json', JSON.stringify(this.answers.entities))
      new EntityBuilder(this.entity).build();
    }
  }

  async addAttribute() {
    const attribute = {};

    const nameRes = await this.prompt({
      type: 'input',
      name: 'name',
      message: 'Attribute Name'
    });

    attribute.name = nameRes.name

    const typeRes = await this.prompt({
      type: 'list',
      name: 'type',
      choices: ['varchar', 'numeric', 'text', 'boolean', 'date', 'timestamp', 'datetime'],
      message: 'Atrribute type'
    })

    attribute.type = typeRes.type
    if (attribute.type === 'varchar') {

      const charLength = await this.prompt({
        type: 'input',
        name: 'length',
        message: 'Enter Attribute length',
        default: 255
      });

      attribute.length = charLength.length


    }
    const opts = await this.prompt({
      type: 'list',
      name: 'type',
      choices: ['not null', ],
      message: 'Atrribute options'
    })
    return attribute

  }
}