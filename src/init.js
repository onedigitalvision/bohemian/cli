import DetectPlatform from "./utils/detect-platform";
import Logger from "./utils/logger";
import CLI from "./cli/cli";

const path = '/Users/ant/codebase/artists/cms/backend'
export default class Init {

  init() {
    Logger.info('Initialising CLI')
    
    const detective = new DetectPlatform();
    if (detective.detect(path)) {
      const cli = new CLI();
      cli.init();
    }
  }
}