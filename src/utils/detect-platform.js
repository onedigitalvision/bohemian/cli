import Logger from "./logger";

const fs = require('fs');

export default class DetectPlatform {
  detect(path) {
    Logger.info ('Detecting Platform with path '+ path);

    if (fs.existsSync(path)) {
      Logger.debug('root path exists');

      if(fs.existsSync(`${path}/src/modules/data/data.module.ts`)) {
        Logger.debug('data mod exists');
        return true;
      } 
    
    }
  } 
}