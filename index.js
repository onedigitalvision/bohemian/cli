'use strict';

var fs = require('fs');

const winston =  require('winston');

const Logger = winston.createLogger({
  level: 'debug',
  format: winston.format.json(),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});
 
//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
// 
if (process.env.NODE_ENV !== 'production') {
  Logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }));
}

const fs$1 = require('fs');

class DetectPlatform {
  detect(path) {
    Logger.info ('Detecting Platform with path '+ path);

    if (fs$1.existsSync(path)) {
      Logger.debug('root path exists');

      if(fs$1.existsSync(`${path}/src/modules/data/data.module.ts`)) {
        Logger.debug('data mod exists');
        return true;
      } 
    
    }
  } 
}

const nunjucks = require('nunjucks');
const NunjucksInspect = require('nunjucks-inspect');
 
const  env = new nunjucks.Environment();
env.addExtension('inspect', new NunjucksInspect());
class EntityBuilder {

  constructor(entity) {
    this.entity = entity;


  }

  build() {
    this.constructModule();
    this.constructModel();
    this.constructService();
    this.constructPage();
  }

  constructModule() {
    nunjucks.render('templates/nestjs/{entity}/{entity}.module.ts.njk', this.entity, (err, res) => {
      const dir = `payload/nest/${this.entity.name}/`;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir,{recursive: true});
      }
      fs.writeFileSync(`${dir}/${this.entity.name}.module.ts`, res);
    });
  }

  constructModel() {
    nunjucks.render('templates/nestjs/{entity}/model/{entity}.entity.ts.njk', this.entity, (err, res) => {      
      const dir = `payload/nest/${this.entity.name}/model`;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir,{recursive: true});
      }
      fs.writeFileSync(`${dir}/${this.entity.name}.entity.ts`, res);
    });
  }

  constructService() {
    nunjucks.render('templates/nestjs/{entity}/{entity}/{entity}.service.ts.njk', this.entity, (err, res) => {
      const dir = `payload/nest/${this.entity.name}/${this.entity.name}`;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir,{recursive: true});
      }
      fs.writeFileSync(`${dir}/${this.entity.name}.service.ts`, res);
    });
  }

  constructPage() {
    console.dir(this.entity);
    nunjucks.render('templates/react/{entity}.page.jsx.njk', this.entity, (err, res) => {
      console.error(err);
      console.dir(res);
      const dir = `payload/react/${this.entity.name}`;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir,{recursive: true});
      }
      fs.writeFileSync(`${dir}/${this.entity.name}.jsx`, res);
    });
  }
}

class EntityGenerator {

  constructor(prompt, answers, cli) {
    this.answers = answers;
    this.prompt = prompt;
    this.cli = cli;
    this.entity = {};
  }

  async init() {

    const entityRes = await this.prompt({
      type: 'input',
      name: 'entity',
      message: 'What is the new entity called?'
    });

    this.entity.name = entityRes.entity;

    this.entity.attributes = [];

    this.addAttributes();
    
  }

  async addAttributes() {
    if (this.entity.attributes.length === 0) {
      this.entity.attributes.push(await this.addAttribute());
    }
    const res = await this.prompt({
      type: 'confirm',
      name: 'addAttr',
      default: true,
      message: 'Add another attriibute?'
    });
    if (res.addAttr) {
      this.entity.attributes.push(await this.addAttribute());
      this.addAttributes();
    } else {
      this.answers.entities.push(this.entity);
      fs.writeFileSync('bohemian.json', JSON.stringify(this.answers.entities));
      new EntityBuilder(this.entity).build();
    }
  }

  async addAttribute() {
    const attribute = {};

    const nameRes = await this.prompt({
      type: 'input',
      name: 'name',
      message: 'Attribute Name'
    });

    attribute.name = nameRes.name;

    const typeRes = await this.prompt({
      type: 'list',
      name: 'type',
      choices: ['varchar', 'numeric', 'text', 'boolean', 'date', 'timestamp', 'datetime'],
      message: 'Atrribute type'
    });

    attribute.type = typeRes.type;
    if (attribute.type === 'varchar') {

      const charLength = await this.prompt({
        type: 'input',
        name: 'length',
        message: 'Enter Attribute length',
        default: 255
      });

      attribute.length = charLength.length;


    }
    const opts = await this.prompt({
      type: 'list',
      name: 'type',
      choices: ['not null', ],
      message: 'Atrribute options'
    });
    return attribute

  }
}

const {
  prompt
} = require('inquirer');

class CLI {
  constructor() {
    this.answers = {
      entities: []
    }; 
  }
  
  async init() {
    console.log(`%c
    _______             __                                __                             ______   __        ______ 
   |       |           |  |                              |  |                           /      | |  |      |      |
   | $$$$$$$  ______  |  $$____    ______   ______ ____   |$$  ______   _______        |  $$$$$$|| $$       |$$$$$$
   | $$__/ $$ /      | | $$    |  /      | |      |    | |  | |      | |       |       | $$   |$$| $$        | $$  
   | $$    $$|  $$$$$$|| $$$$$$$||  $$$$$$|| $$$$$$|$$$$|| $$  |$$$$$$|| $$$$$$$|      | $$      | $$        | $$  
   | $$$$$$$|| $$  | $$| $$  | $$| $$    $$| $$ | $$ | $$| $$ /      $$| $$  | $$      | $$   __ | $$        | $$  
   | $$__/ $$| $$__/ $$| $$  | $$| $$$$$$$$| $$ | $$ | $$| $$|  $$$$$$$| $$  | $$      | $$__/  || $$_____  _| $$_ 
   | $$    $$ |$$    $$| $$  | $$ |$$     || $$ | $$ | $$| $$ |$$    $$| $$  | $$       |$$    $$| $$     ||   $$ |
   | $$$$$$$  |$$$$$$  |$$   |$$  |$$$$$$$ |$$  |$$  |$$ |$$  |$$$$$$$ |$$   |$$        |$$$$$$  |$$$$$$$$ |$$$$$$
                                                                                                                                                     
   `);

   console.dir(this.answers);
   const menu = await prompt({
    type: 'list',
    name: 'action',
    choices: ['generate an entity', 'generate an api end point', 'generate the cms crud', 'exit'],
    message: 'I would like to?'
  });

  switch (menu.action) {
    case 'generate an entity':
      new EntityGenerator(prompt, this.answers, this).init();
  }

    
  }

}

const path = '/Users/ant/codebase/artists/cms/backend';
class Init {

  init() {
    Logger.info('Initialising CLI');
    
    const detective = new DetectPlatform();
    if (detective.detect(path)) {
      const cli = new CLI();
      cli.init();
    }
  }
}

const start = new Init();

start.init();
